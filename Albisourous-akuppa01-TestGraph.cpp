// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>
//          Graph // uncomment, add a comma to the line above
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// Set of given tests
TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vert1);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    edge_descriptor edAB = add_edge(vert1, vert2, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vert1, vert2, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vert1, vert2, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vert1);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vert2);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vert1);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vert2);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vert1, vert2, g).first;
    edge_descriptor edAC = add_edge(vert1, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vert1, vert2, g);
    add_edge(vert1, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vert1, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vert2);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// Test edge

TYPED_TEST(GraphFixture, edgeTest0) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    ASSERT_TRUE(add_edge(add_vertex(g), add_vertex(g), g).second);
}

TYPED_TEST(GraphFixture, edgeTest1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    const auto edge1 = add_edge(vert1, vert2, g);
    ASSERT_TRUE(edge1.second);
    const auto edge2 = add_edge(vert1, vert2, g);
    ASSERT_FALSE(edge2.second);

    ASSERT_EQ(edge1.first, edge2.first);
}

TYPED_TEST(GraphFixture, edgeTest2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    edge_descriptor edge1 = add_edge(vert1, vert2, g).first;
    edge_descriptor edge2 = add_edge(vert1, vert3, g).first;

    ASSERT_NE(edge1, edge2);
}

TYPED_TEST(GraphFixture, edgeTest3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    edge_descriptor edge1 = add_edge(vert1, vert2, g).first;
    edge_descriptor edge2 = add_edge(vert1, vert3, g).first;


    pair<edge_iterator, edge_iterator> pair = edges(g);
    edge_iterator first = pair.first;
    edge_iterator second = pair.second;
    ASSERT_NE(first, second);
}

TYPED_TEST(GraphFixture, edgeTest4) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    edge_descriptor ed12 = add_edge(vert1, vert2, g).first;
    edge_descriptor ed13 = add_edge(vert1, vert3, g).first;
    edge_descriptor ed21 = add_edge(vert2, vert1, g).first;
    edge_descriptor ed23 = add_edge(vert2, vert3, g).first;
    edge_descriptor ed31 = add_edge(vert3, vert1, g).first;
    edge_descriptor ed32 = add_edge(vert3, vert2, g).first;

    ASSERT_NE(ed12, ed21);
    ASSERT_NE(ed13, ed31);
    ASSERT_NE(ed23, ed32);
}

TYPED_TEST(GraphFixture, edgeTest5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    const auto edge1 = add_edge(vert1, vert2, g);
    const auto edge2 = add_edge(vert2, vert1, g);
    ASSERT_TRUE(edge1.second);
    ASSERT_TRUE(edge2.second);
}

TYPED_TEST(GraphFixture, edgeTest6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);
    ASSERT_TRUE(added);
}


TYPED_TEST(GraphFixture, edgeTest7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);
    ASSERT_TRUE(edge(vert1, vert2, g).second);
}

TYPED_TEST(GraphFixture, edgeTest8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);
    ASSERT_EQ(ed, edge(vert1, vert2, g).first);
}

TYPED_TEST(GraphFixture, edgeTest9) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    edge_descriptor ed12 = add_edge(vert1, vert2, g).first;
    edge_descriptor ed13 = add_edge(vert1, vert3, g).first;
    edge_descriptor ed21 = add_edge(vert2, vert1, g).first;
    edge_descriptor ed23 = add_edge(vert2, vert3, g).first;
    edge_descriptor ed31 = add_edge(vert3, vert1, g).first;
    edge_descriptor ed32 = add_edge(vert3, vert2, g).first;

    ASSERT_NE(ed12, ed21);
    ASSERT_NE(ed13, ed31);
    ASSERT_NE(ed23, ed32);

    ASSERT_NE(ed12, ed31);
    ASSERT_NE(ed13, ed32);
    ASSERT_NE(ed23, ed21);

    ASSERT_NE(ed12, ed21);
    ASSERT_NE(ed13, ed32);
    ASSERT_NE(ed23, ed31);
}

TYPED_TEST(GraphFixture, edgeTest10) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    edge_descriptor edge1 = add_edge(vert1, vert2, g).first;
    edge_descriptor edge2 = add_edge(vert1, vert3, g).first;

    pair<edge_iterator, edge_iterator> pair = edges(g);
    edge_iterator first = pair.first;
    edge_iterator second = pair.second;
    ASSERT_NE(edge1, edge2);
}

TYPED_TEST(GraphFixture, edgeTest11) {
    typedef typename TestFixture::graph_type         graph_type;
    typedef typename TestFixture::vertex_descriptor  vertex_descriptor;
    typedef typename TestFixture::edge_descriptor    edge_descriptor;


    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    edge_descriptor edAB = add_edge(vert1, vert2, g).first;

    pair<edge_descriptor, bool> p1 = edge(vert1, vert2, g);
    ASSERT_EQ(edAB, p1.first);
    ASSERT_EQ(true, p1.second);

    pair<edge_descriptor, bool> p2 = edge(vert2, vert1, g);
    ASSERT_TRUE(edAB != p2.first);
    ASSERT_EQ(false, p2.second);
}

TYPED_TEST(GraphFixture, edgeTest12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    auto [ed, added] = add_edge(vert1, vert3, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);

    auto [ed, added] = add_edge(vert2, vert3, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert3, vert2, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert3, vert1, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert2, vert1, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert1, vert4, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert1, vert5, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert2, vert4, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert5, vert3, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert4, vert1, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert4, vert2, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert4, vert3, g);
    ASSERT_TRUE(added);
}

TYPED_TEST(GraphFixture, edgeTest24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    vertex_descriptor vert3 = add_vertex(g);
    vertex_descriptor vert4 = add_vertex(g);
    vertex_descriptor vert5 = add_vertex(g);

    auto [ed, added] = add_edge(vert4, vert5, g);
    ASSERT_TRUE(added);
}

// Test vertex

TYPED_TEST(GraphFixture, vertexTest0) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, vertexTest1) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 3);
}

TYPED_TEST(GraphFixture, vertexTest2) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 6);
}

TYPED_TEST(GraphFixture, vertexTest3) {
    using graph_type          = typename TestFixture::graph_type;
    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 12);
}

TYPED_TEST(GraphFixture, vertexTest4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
}

TYPED_TEST(GraphFixture, vertexTest5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    ASSERT_NE(vert1, vert2);
}

TYPED_TEST(GraphFixture, vertexTest6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
}

TYPED_TEST(GraphFixture, vertexTest7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    std::vector<vertex_descriptor> vec;
    vec.push_back(vertex(0, g));
    vec.push_back(vertex(1, g));
    ASSERT_EQ(vec.size(), 2);
    ASSERT_TRUE(find(vec.begin(), vec.end(), vert1) != vec.end());
}

TYPED_TEST(GraphFixture, vertexTest8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
    std::vector<vertex_descriptor> vec;
    vec.push_back(vertex(0, g));
    vec.push_back(vertex(1, g));
    ASSERT_TRUE(find(vec.begin(), vec.end(), vert1) != vec.end());
}

TYPED_TEST(GraphFixture, vertexTest9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
    std::vector<vertex_descriptor> vec;
    vec.push_back(vertex(0, g));
    vec.push_back(vertex(1, g));
    ASSERT_TRUE(find(vec.begin(), vec.end(), vert2) != vec.end());
}

TYPED_TEST(GraphFixture, vertexTest10) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);

    vertices_size_type size = num_vertices(g);
    ASSERT_EQ(size, 1);

}

TYPED_TEST(GraphFixture, vertexTest11) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vert1);
}

// Test the target and source

TYPED_TEST(GraphFixture, sourceTest0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, _] = add_edge(vert1, vert2, g);
    ASSERT_EQ(source(ed, g), vert1);
}

TYPED_TEST(GraphFixture, targetTest0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, _] = add_edge(vert1, vert2, g);
    ASSERT_EQ(target(ed, g), vert2);
}

// Test adjacency

TYPED_TEST(GraphFixture, adjTest0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vert1 = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, adjTest1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;
    vertex_descriptor vert1 = add_vertex(g);

    auto [b, e] = adjacent_vertices(vert1, g);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, adjTest2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [b, e] = adjacent_vertices(vert1, g);
    ASSERT_EQ(*b, vert2);
}

TYPED_TEST(GraphFixture, adjTest3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [b, e] = adjacent_vertices(vert1, g);
    ASSERT_TRUE(b != e);
}

TYPED_TEST(GraphFixture, adjTest4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vert1 = add_vertex(g);
    vertex_descriptor vert2 = add_vertex(g);
    auto [ed, added] = add_edge(vert1, vert2, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [b, e] = adjacent_vertices(vert1, g);
    ASSERT_EQ(std::distance(b, e), 1);
}


