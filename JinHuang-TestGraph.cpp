// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment, add a comma to the line above
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// Test add vertx, add edge
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edAD = add_edge(vdA, vdD, g);
    pair<edge_descriptor, bool> edAD2 = add_edge(vdA, vdD, g);

    edge_descriptor edDA = add_edge(vdD, vdA, g).first;

    ASSERT_EQ(true, edAB.second);
    ASSERT_EQ(true, edAD.second);
    ASSERT_EQ(false, edAD2.second);
    ASSERT_NE(edAD.first, edDA);
}

// Test vertex descriptor is correct
TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(0,vdA);
    ASSERT_EQ(2,vdC);
}

// Test add_edge and bool
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);

    ASSERT_EQ(true, edAC.second);
    ASSERT_NE(edAB.first, edAC.first);
}

// test add_edge to itself
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    pair<edge_descriptor, bool> edAA = add_edge(vdA, vdA, g);

    ASSERT_EQ(true, edAA.second);
}

// Test num_edges(), num_vertices()
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));
    ASSERT_EQ(0, num_vertices(g));
}

// Test num_edges(), num_vertices()
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);

    ASSERT_EQ(2, num_edges(g));
    ASSERT_EQ(3, num_vertices(g));
}

// Test add_edge with empty vertex
TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    pair<edge_descriptor, bool> edAB = add_edge(5, 9, g);
    ASSERT_EQ(10, num_vertices(g));
    ASSERT_EQ(1, num_edges(g));
}

// Test add_vertex, large number
TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    for (int i = 0; i < 100; i++) {
        vertex_descriptor vd = add_vertex(g);
        ASSERT_EQ(vd, i);
    }
}

// add_edge, cicle
TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edCA = add_edge(vdC, vdA, g);

    ASSERT_EQ(3, num_edges(g));
    ASSERT_EQ(edCA.second, true);
    ASSERT_NE(edCA.first, edAC.first);
}

// adjacent_vertices
TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edAD = add_edge(vdA, vdD, g);
    pair<edge_descriptor, bool> edAE = add_edge(vdA, vdE, g);
    pair<edge_descriptor, bool> edAA = add_edge(vdA, vdA, g);

    ASSERT_EQ(edAA.second, true);
    ASSERT_EQ(num_edges(g), 5);

    adjacency_iterator begin = adjacent_vertices(vdA, g).first;
    adjacency_iterator end = adjacent_vertices(vdA, g).second;
    int count = 0;
    while (begin != end) {
        ASSERT_EQ(*begin, count);
        ++begin;
        ++count;
    }
    ASSERT_EQ(count, 5);
}

// adjacent_vertices
TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdB, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> it = adjacent_vertices(vdA, g);
    adjacency_iterator b = it.first;
    adjacency_iterator e = it.second;

    ASSERT_EQ(*b, vdB);
    b++;
    ASSERT_EQ(*b, vdC);
    b++;
    ASSERT_EQ(b, e);

    pair<adjacency_iterator, adjacency_iterator> it2 = adjacent_vertices(vdB, g);
    adjacency_iterator b2 = it2.first;
    adjacency_iterator e2 = it2.second;

    ASSERT_EQ(*b2, vdA);
    b2++;
    ASSERT_EQ(*b2, vdC);
    b2++;
    ASSERT_EQ(b2, e2);
}


// Test edge
TYPED_TEST(GraphFixture, test16) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edBA = add_edge(vdB, vdA, g);
    pair<edge_descriptor, bool> edCB = add_edge(vdC, vdB, g);
    pair<edge_descriptor, bool> edBC = add_edge(vdB, vdC, g);

    ASSERT_EQ(edge(vdA, vdB, g), edAB);
    ASSERT_EQ(edge(vdA, vdC, g), edAC);
    ASSERT_EQ(edge(vdB, vdA, g), edBA);
    ASSERT_EQ(edge(vdC, vdB, g), edCB);
    ASSERT_EQ(edge(vdB, vdC, g), edBC);
}

// edge
TYPED_TEST(GraphFixture, test17) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> p2 = edge(vdB, vdA, g);

    ASSERT_FALSE(p1.second);
    ASSERT_FALSE(p2.second);
}

// edge
TYPED_TEST(GraphFixture, test18) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    pair<edge_descriptor, bool> p1 = edge(vdA, vdA, g);
    ASSERT_FALSE(p1.second);
}

// edge
TYPED_TEST(GraphFixture, test19) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edBA = add_edge(vdB, vdA, g);
    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> p2 = edge(vdB, vdA, g);

    ASSERT_TRUE(p1.second);
    ASSERT_TRUE(p2.second);
    ASSERT_NE(p1, p2);
}

// edges, large number
TYPED_TEST(GraphFixture, test20) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    for (int i = 0; i < 100; i++) {
        add_vertex(g);
    }
    for (int i = 0; i < 99; i++) {
        add_edge(vertex(i, g), vertex(i + 1, g), g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator begin = p.first;
    edge_iterator end = p.second;

    for (int i = 0; i < 99; i++) {
        begin++;
    }

    ASSERT_EQ(begin, end);
}

// edges
TYPED_TEST(GraphFixture, test21) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edBA = add_edge(vdB, vdA, g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator begin = p.first;
    edge_iterator end = p.second;

    ASSERT_EQ(*begin, edAB.first);
    begin++;
    ASSERT_EQ(*begin, edAC.first);
    begin++;
    ASSERT_EQ(*begin, edBA.first);
    begin++;

    ASSERT_EQ(begin, end);
}

// edges
TYPED_TEST(GraphFixture, test22) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    ASSERT_EQ(num_edges(g), 0);
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> edAA = add_edge(vdA, vdA, g);
    pair<edge_descriptor, bool> edBB = add_edge(vdB, vdB, g);
    pair<edge_descriptor, bool> edAA2 = add_edge(vdA, vdA, g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator begin = p.first;
    edge_iterator end = p.second;

    ASSERT_EQ(*begin, edAA.first);
    begin++;
    ASSERT_EQ(*begin, edBB.first);
    begin++;

    ASSERT_EQ(begin, end);
}

// num_edges
TYPED_TEST(GraphFixture, test23) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    ASSERT_EQ(num_edges(g), 0);
}

// num_vertex
TYPED_TEST(GraphFixture, test24) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
}

// num_vertex
TYPED_TEST(GraphFixture, test25) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 3);
}

// num_edges
TYPED_TEST(GraphFixture, test26) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_edge(0, 1, g);
    add_edge(1, 2, g);
    add_edge(2, 1, g);
    ASSERT_EQ(num_edges(g), 3);
}

// num_edges, num_vertices()
TYPED_TEST(GraphFixture, test27) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_edge(6, 100, g);
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 101);
}

// source
TYPED_TEST(GraphFixture, test28) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool>  edAB = add_edge(vdA, vdB, g);
    ASSERT_EQ(source(edAB.first, g), vdA);
}

// source
TYPED_TEST(GraphFixture, test29) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    pair<edge_descriptor, bool>  edAA = add_edge(vdA, vdA, g);
    ASSERT_EQ(source(edAA.first, g), vdA);
}

// target
TYPED_TEST(GraphFixture, test30) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool>  edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool>  edBA = add_edge(vdB, vdA, g);
    ASSERT_EQ(target(edAB.first, g), vdB);
    ASSERT_EQ(target(edBA.first, g), vdA);
}

//Test vertex
TYPED_TEST(GraphFixture, test31) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    vertex_descriptor v1  = vertex(2, g);
    ASSERT_EQ(v1, vdC);
}



// vertex
TYPED_TEST(GraphFixture, test33) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor v0  = vertex(0, g);
    vertex_descriptor v1  = vertex(1, g);
    ASSERT_EQ(vdA, v0);
    ASSERT_EQ(vdB, v1);
}

TYPED_TEST(GraphFixture, test34) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator  = typename graph_type::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                           b = p.first;
    vertex_iterator                           e = p.second;
    ASSERT_EQ(e, b);
}

// vertices, large number
TYPED_TEST(GraphFixture, test35) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator  = typename graph_type::vertex_iterator;

    graph_type g;

    for (int i = 0; i < 100; i++) {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator begin = p.first;
    vertex_iterator end = p.second;

    for (int i = 0; i < 100; i++) {
        begin++;
    }

    ASSERT_EQ(begin, end);
}

// vertices
TYPED_TEST(GraphFixture, test36) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_iterator  = typename graph_type::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator begin = p.first;
    vertex_iterator end = p.second;

    ASSERT_EQ(*begin, vdA);
    begin++;
    ASSERT_EQ(*begin, vdB);
    begin++;
    ASSERT_EQ(*begin, vdC);
    begin++;
    ASSERT_EQ(*begin, vdD);
    begin++;
    ASSERT_EQ(begin, end);
}

// complicated cases, adjacent_vertices
TYPED_TEST(GraphFixture, test37) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edBC = add_edge(vdB, vdC, g);
    pair<edge_descriptor, bool> edCB = add_edge(vdC, vdB, g);
    pair<edge_descriptor, bool> edCA = add_edge(vdC, vdA, g);
    pair<edge_descriptor, bool> edCD = add_edge(vdC, vdD, g);
    pair<edge_descriptor, bool> edDA = add_edge(vdD, vdA, g);
    pair<edge_descriptor, bool> edAD = add_edge(vdA, vdD, g);

    ASSERT_EQ(num_vertices(g), 4);
    ASSERT_EQ(num_edges(g), 7);
    ASSERT_EQ(edCD.second,true);

    adjacency_iterator begin = adjacent_vertices(vdC, g).first;
    adjacency_iterator end = adjacent_vertices(vdC, g).second;
    int count = 0;
    while (begin != end) {
        ++begin;
        ++count;
    }
    ASSERT_EQ(count, 3);
}

// Test edge, add_edge
TYPED_TEST(GraphFixture, test38) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);

    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_FALSE(edge(vdB, vdA, g).second);
    ASSERT_TRUE(edge(vdC, vdA, g).second);
}

// Two different graph
TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);

    add_edge(vdA, vdB, g1);
    ASSERT_EQ(num_edges(g1), 1);
}

// complicated add_edge
TYPED_TEST(GraphFixture, test40) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> edAC = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> edBA = add_edge(vdB, vdA, g);
    add_edge(2, 10, g);

    ASSERT_EQ(num_edges(g), 4);
    ASSERT_EQ(num_vertices(g), 11);
}

