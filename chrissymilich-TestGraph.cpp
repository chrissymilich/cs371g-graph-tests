// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment, add a comma to the line above
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// add vertex, single vertex
TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

// add edge, single edge, return false if already there
TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

// vertex iterator
TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

// edge iterator
TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

// adjacenecy iterator
TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// edge iterator order
TYPED_TEST(GraphFixture, test5) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBA);
    ++b;
    ASSERT_EQ(e, b);
}

// multiple added vertices, size correct
TYPED_TEST(GraphFixture, test6) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vd = vertex(1, g);
    ASSERT_EQ(vd, vdB);

    vd = vertex(2, g);
    ASSERT_EQ(vd, vdC);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 3u);
}

// multiple added edges, no bidrection
TYPED_TEST(GraphFixture, test7) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, true);

    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    pair<edge_descriptor, bool> p2 = edge(vdA, vdC, g);
    ASSERT_EQ(p2.first,  edAC);
    ASSERT_EQ(p2.second, true);

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    pair<edge_descriptor, bool> p3 = edge(vdB, vdC, g);
    ASSERT_EQ(p3.first,  edBC);
    ASSERT_EQ(p3.second, true);
}

// multiple added edges, bidirection
TYPED_TEST(GraphFixture, test8) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, true);

    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    pair<edge_descriptor, bool> p2 = edge(vdA, vdC, g);
    ASSERT_EQ(p2.first,  edAC);
    ASSERT_EQ(p2.second, true);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    pair<edge_descriptor, bool> p3 = edge(vdB, vdA, g);
    ASSERT_EQ(p3.first,  edBA);
    ASSERT_EQ(p3.second, true);

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    pair<edge_descriptor, bool> p4 = edge(vdB, vdC, g);
    ASSERT_EQ(p4.first,  edBC);
    ASSERT_EQ(p4.second, true);

    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    pair<edge_descriptor, bool> p5 = edge(vdC, vdA, g);
    ASSERT_EQ(p5.first,  edCA);
    ASSERT_EQ(p5.second, true);

    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    pair<edge_descriptor, bool> p6 = edge(vdC, vdB, g);
    ASSERT_EQ(p6.first,  edCB);
    ASSERT_EQ(p6.second, true);
}

// multiple added edges, bidirection, source and target
TYPED_TEST(GraphFixture, test9) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);
    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);

    vd1 = source(edAC, g);
    ASSERT_EQ(vd1, vdA);
    vd2 = target(edAC, g);
    ASSERT_EQ(vd2, vdC);

    vd1 = source(edBA, g);
    ASSERT_EQ(vd1, vdB);
    vd2 = target(edBA, g);
    ASSERT_EQ(vd2, vdA);

    vd1 = source(edBC, g);
    ASSERT_EQ(vd1, vdB);
    vd2 = target(edBC, g);
    ASSERT_EQ(vd2, vdC);

    vd1 = source(edCA, g);
    ASSERT_EQ(vd1, vdC);
    vd2 = target(edCA, g);
    ASSERT_EQ(vd2, vdA);

    vd1 = source(edCB, g);
    ASSERT_EQ(vd1, vdC);
    vd2 = target(edCB, g);
    ASSERT_EQ(vd2, vdB);
}

// add vertex, no edges, adjacency iterator
TYPED_TEST(GraphFixture, test10) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_EQ(b, e);

    vertex_descriptor vdB = add_vertex(g);
    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(b, e);
}

// add edge w non existent vertices
TYPED_TEST(GraphFixture, test11) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g); // 0
    vertex_descriptor vdB = add_vertex(g); // 1

    vdA += 3; // vdA = 3
    vdB += 5; // vdB = 6

    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);

    ASSERT_TRUE(p.second);
    ASSERT_EQ(num_edges(g), 1u);
    ASSERT_EQ(num_vertices(g), 7u);
}

// add edge w 1 non existent vertex
TYPED_TEST(GraphFixture, test12) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g); // 0
    vertex_descriptor vdB = vdA + 100;

    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);

    ASSERT_TRUE(p.second);
    ASSERT_EQ(num_edges(g), 1u);
    ASSERT_EQ(num_vertices(g), 101u);
}

// empty graph add edge
TYPED_TEST(GraphFixture, test13) {
    using graph_type         = typename TestFixture::graph_type;

    graph_type g;
    add_edge(0, 9, g);

    ASSERT_EQ(num_edges(g), 1u);
    ASSERT_EQ(num_vertices(g), 10u);
}

// adding edge from non existent vertices, adjacency
TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    add_edge(0, 1, g);

    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 2);

    auto p = adjacent_vertices(0, g);

    ASSERT_EQ(*p.first, 1);
    p = adjacent_vertices(1, g);
    ASSERT_EQ(*p.first, 0);
}

// non existent edge, source
TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = 0;
    vertex_descriptor vdB = 1;
    add_vertex(g);
    add_vertex(g);

    pair<edge_descriptor, bool> e = edge(vdA, vdB, g);

    ASSERT_EQ(source(e.first, g), vdA);

}

// non existent edge, target
TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = 0;
    vertex_descriptor vdB = 1;
    add_vertex(g);
    add_vertex(g);

    pair<edge_descriptor, bool> e = edge(vdA, vdB, g);

    ASSERT_EQ(target(e.first, g), vdB);

}

// default
TYPED_TEST(GraphFixture, test17) {
    using graph_type         = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0u);
    ASSERT_EQ(num_edges(g), 0u);
}

// vertex iterator, order
TYPED_TEST(GraphFixture, test18) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(0, 9, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd = *b;
    ASSERT_EQ(vd, vdA);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, vdB);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 2);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 3);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 4);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 5);
    ++b;
    ASSERT_NE(b, e);
    vd = *b;

    ASSERT_EQ(vd, 6);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 7);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 8);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 9);
    ++b;
    ASSERT_EQ(b, e);
}

// vertex iterator, order, from only add edge
TYPED_TEST(GraphFixture, test19) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    add_edge(0, 9, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd = *b;
    ASSERT_EQ(vd, 0);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 1);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 2);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 3);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 4);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 5);
    ++b;
    ASSERT_NE(b, e);
    vd = *b;

    ASSERT_EQ(vd, 6);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 7);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 8);
    ++b;
    ASSERT_NE(b, e);

    vd = *b;
    ASSERT_EQ(vd, 9);
    ++b;
    ASSERT_EQ(b, e);
}

// edge iterator, add edge
TYPED_TEST(GraphFixture, test20) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    edge_descriptor ed = add_edge(0, 9, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, ed);
    ++b;
    ASSERT_EQ(b, e);
}

// add edge, return true
TYPED_TEST(GraphFixture, test21) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.second, true);
}

// adjacenecy iterator, add edge
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    for (int i = 1; i < 4; i++) {
        add_edge(0, i, g);
        add_edge(i, 0,g);
    }

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, 1);
    ++b;
    ASSERT_NE(b, e);

    vd1 = *b;
    ASSERT_EQ(vd1, 2);
    ++b;
    ASSERT_NE(b, e);

    vd1 = *b;
    ASSERT_EQ(vd1, 3);
    ++b;
    ASSERT_EQ(b, e);
}

// add edge to self, edge iterator
TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    edge_descriptor ed = add_edge(0, 0, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, ed);
    ++b;
    ASSERT_EQ(b, e);
}

// add edge to self, adjacency iterator
TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    add_edge(0, 0, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0, g);
    adjacency_iterator                      b = p.first;
    adjacency_iterator                      e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, 0);
    ++b;
    ASSERT_EQ(b, e);
}

// add edge to self, vertex iterator
TYPED_TEST(GraphFixture, test25) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    add_edge(0, 0, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, 0);
    ++b;
    ASSERT_EQ(b, e);
}

// add edge to self, return true
TYPED_TEST(GraphFixture, test26) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    pair<edge_descriptor, bool> p = add_edge(0, 0, g);

    ASSERT_EQ(p.second, true);

}

// add edge to self, edge
TYPED_TEST(GraphFixture, test27) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    pair<edge_descriptor, bool> p1 = add_edge(0, 0, g);
    pair<edge_descriptor, bool> p2 = edge(0, 0, g);

    ASSERT_EQ(p2.first, p1.first);
    ASSERT_EQ(p2.second, true);

}

// add edge to self, return false
TYPED_TEST(GraphFixture, test28) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    pair<edge_descriptor, bool> p1 = add_edge(0, 0, g);
    pair<edge_descriptor, bool> p2 = add_edge(0, 0, g);

    ASSERT_EQ(p2.first, p1.first);
    ASSERT_EQ(p2.second, false);

}

// big test, vertices
TYPED_TEST(GraphFixture, test29) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 10000; i++) {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 10000);

}

// big test, edges
TYPED_TEST(GraphFixture, test30) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 10000; i++) {
        add_vertex(g);
        add_edge(0, i, g);
    }
    ASSERT_EQ(num_edges(g), 10000);
}

// big test, adjacency iterator
TYPED_TEST(GraphFixture, test31) {
    using graph_type        = typename TestFixture::graph_type;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 10000; i++) {
        add_vertex(g);
        add_edge(0, i, g);
    }

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(0, g);
    adjacency_iterator                      b = p.first;
    adjacency_iterator                      e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd;
    int count = 0;
    while (b != e) {
        vd = *b;
        ASSERT_EQ(vd, count);
        ++b;
        count++;
    }

    ASSERT_EQ(b, e);
}

// big test, vertex iterator
TYPED_TEST(GraphFixture, test32) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator  = typename TestFixture::vertex_iterator;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 10000; i++) {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                      b = p.first;
    vertex_iterator                      e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd;
    int count = 0;
    while (b != e) {
        vd = *b;
        ASSERT_EQ(vd, count);
        ++b;
        count++;
    }

    ASSERT_EQ(b, e);
}

// big test, edge iterator
TYPED_TEST(GraphFixture, test33) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_iterator  = typename TestFixture::edge_iterator;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    for (int i = 0; i < 10000; i++) {
        add_edge(0, i, g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed;
    int count = 0;
    while (b != e) {
        ed = *b;
        pair<edge_descriptor, bool> p = edge(0, count, g);
        ASSERT_EQ(ed, p.first);
        ASSERT_EQ(true, p.second);
        ++b;
        count++;
    }
    ASSERT_EQ(count, 10000);
    ASSERT_EQ(b, e);
}

// vertex(), but not there, starting empty
TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vd = vertex(42, g);
    ASSERT_EQ(vd, 42);
    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(num_edges(g), 0);
}

// vertex(), but not there, not empty
TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor vd = vertex(70, g);
    ASSERT_EQ(vd, 70);
    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_EQ(num_edges(g), 0);
}

// edge iterator, epmty graph
TYPED_TEST(GraphFixture, test36) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_EQ(b, e);
}

// vertex iterator, epmty graph
TYPED_TEST(GraphFixture, test37) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                      b = p.first;
    vertex_iterator                      e = p.second;
    ASSERT_EQ(b, e);
}

// adding edge from non existent vertices, vertex iterator
TYPED_TEST(GraphFixture, test38) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    add_edge(0, 1, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                      b = p.first;
    vertex_iterator                      e = p.second;
    ASSERT_NE(b, e);

    ASSERT_EQ(*b, 0);
    b++;
    ASSERT_EQ(*b, 1);
    b++;
    ASSERT_EQ(b, e);
}

// adding edge from non existent vertices, edge iterator
TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;
    pair<edge_descriptor, bool> p1 = add_edge(0, 1, g);

    pair<edge_iterator, edge_iterator> p2 = edges(g);
    edge_iterator                      b = p2.first;
    edge_iterator                      e = p2.second;
    ASSERT_NE(b, e);

    ASSERT_EQ(*b, p1.first);
    b++;
    ASSERT_EQ(b, e);
}

// multiple graphs, add vertex
TYPED_TEST(GraphFixture, test40) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor v1 = add_vertex(g1);
    vertex_descriptor v2 = add_vertex(g2);
    ASSERT_EQ(v1, v2);
    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_vertices(g2), 1);

    vertex_descriptor v_test = vertex(v1, g2);
    ASSERT_EQ(v_test, v2);

    v_test = vertex(v2, g1);
    ASSERT_EQ(v_test, v1);
}

// edge iterator order, more complex
TYPED_TEST(GraphFixture, test41) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBA);
    ++b;
    ASSERT_NE(e, b);

    edge_descriptor ed4 = *b;
    ASSERT_EQ(ed4, edBC);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed5 = *b;
    ASSERT_EQ(ed5, edCA);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed6 = *b;
    ASSERT_EQ(ed6, edCB);
    ++b;
    ASSERT_EQ(b, e);
}

// adjacency iterator order, more complex
TYPED_TEST(GraphFixture, test42) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator     = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdC, vdB, g).first;
    add_edge(vdA, vdB, g).first;
    add_edge(vdB, vdC, g).first;
    add_edge(vdA, vdC, g).first;
    add_edge(vdC, vdA, g).first;
    add_edge(vdB, vdA, g).first;

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                      b = p.first;
    adjacency_iterator                      e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor v = *b;
    ASSERT_EQ(v, vdB);
    ++b;
    ASSERT_NE(b, e);

    v = *b;
    ASSERT_EQ(v, vdC);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;
    ASSERT_NE(b, e);

    v = *b;
    ASSERT_EQ(v, vdA);
    ++b;
    ASSERT_NE(b, e);

    v = *b;
    ASSERT_EQ(v, vdC);
    ++b;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;
    ASSERT_NE(b, e);

    v = *b;
    ASSERT_EQ(v, vdA);
    ++b;
    ASSERT_NE(b, e);

    v = *b;
    ASSERT_EQ(v, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

// multiple added edges, no bidrection, adjacenecy correct
TYPED_TEST(GraphFixture, test43) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator     = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g).first;
    add_edge(vdA, vdC, g).first;
    add_edge(vdB, vdC, g).first;

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(*b, vdB);
    b++;
    ASSERT_EQ(*b, vdC);
    b++;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;

    ASSERT_EQ(*b, vdC);
    b++;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;
    ASSERT_EQ(b, e);
}

// multiple added edges, bidrection, adjacenecy correct
TYPED_TEST(GraphFixture, test44) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator     = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g).first;
    add_edge(vdA, vdC, g).first;
    add_edge(vdB, vdC, g).first;
    add_edge(vdB, vdA, g).first;
    add_edge(vdC, vdA, g).first;
    add_edge(vdC, vdB, g).first;

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(*b, vdB);
    b++;
    ASSERT_EQ(*b, vdC);
    b++;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdB, g);
    b = p.first;
    e = p.second;

    ASSERT_EQ(*b, vdA);
    b++;
    ASSERT_EQ(*b, vdC);
    b++;
    ASSERT_EQ(b, e);

    p = adjacent_vertices(vdC, g);
    b = p.first;
    e = p.second;

    ASSERT_EQ(*b, vdA);
    b++;
    ASSERT_EQ(*b, vdB);
    b++;
    ASSERT_EQ(b, e);
}

// multiple graphs, add edge
TYPED_TEST(GraphFixture, test45) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor g1_v1 = 0;
    vertex_descriptor g1_v2 = 3;
    vertex_descriptor g2_v1 = 0;
    vertex_descriptor g2_v2 = 3;

    add_edge(g1_v1, g1_v2, g1);
    add_edge(g2_v1, g2_v2, g2);

    pair<edge_descriptor, bool> p1_test = edge(g1_v1, g1_v2, g2);
    ASSERT_EQ(p1_test.second, true);
    ASSERT_EQ(target(p1_test.first, g1), g1_v2);
    ASSERT_EQ(target(p1_test.first, g1), g2_v2);
    ASSERT_EQ(source(p1_test.first, g1), g1_v1);
    ASSERT_EQ(source(p1_test.first, g1), g2_v1);

    pair<edge_descriptor, bool> p2_test = edge(g2_v1, g2_v2, g1);
    ASSERT_EQ(p2_test.second, true);
    ASSERT_EQ(target(p1_test.first, g2), g1_v2);
    ASSERT_EQ(target(p1_test.first, g2), g2_v2);
    ASSERT_EQ(source(p1_test.first, g2), g1_v1);
    ASSERT_EQ(source(p1_test.first, g2), g2_v1);

}
