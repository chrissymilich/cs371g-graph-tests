// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream>   // cout, endl
#include <iterator>   // ostream_iterator
#include <sstream>    // ostringstream
#include <utility>    // pair
#include <stdlib.h>   // srand, rand
#include <algorithm>  // find

#include "boost/graph/adjacency_list.hpp" // Adjacency list
#include "boost/graph/breadth_first_search.hpp" // BFS
#include "boost/graph/depth_first_search.hpp" // DFS
#include "boost/graph/topological_sort.hpp" // Topological sort
#include "boost/graph/strong_components.hpp" // Strong / Connected Components

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;
using namespace boost;
template < typename TimeMap > class dfs_time_visitor:public default_dfs_visitor {
    typedef typename property_traits < TimeMap >::value_type T;
public:
    dfs_time_visitor(TimeMap dmap, TimeMap fmap, T & t)
        :  m_dtimemap(dmap), m_ftimemap(fmap), m_time(t) {
    }
    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex u, const Graph & g) const
    {
        assert(num_vertices(g) > 0);
        put(m_dtimemap, u, m_time++);
    }
    template < typename Vertex, typename Graph >
    void finish_vertex(Vertex u, const Graph & g) const
    {
        assert(num_vertices(g) > 0);
        put(m_ftimemap, u, m_time++);
    }
    TimeMap m_dtimemap;
    TimeMap m_ftimemap;
    T & m_time;
};

template < typename TimeMap > class bfs_time_visitor:public default_bfs_visitor {
    typedef typename property_traits < TimeMap >::value_type T;
public:
    bfs_time_visitor(TimeMap tmap, T & t):m_timemap(tmap), m_time(t) { }
    template < typename Vertex, typename Graph >
    void discover_vertex(Vertex u, const Graph & g) const
    {
        assert(num_vertices(g) > 0);
        put(m_timemap, u, m_time++);
    }
    TimeMap m_timemap;
    T & m_time;
};


// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment, add a comma to the line above
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test5) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    vertex_descriptor vd0 = vertex(0, g);
    vertex_descriptor vd1 = vertex(1, g);
    ASSERT_EQ(vd0, vdA);
    ASSERT_EQ(vd1, vdB);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2u);
}

TYPED_TEST(GraphFixture, test6) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = add_edge(vdB, vdC, g);
    ASSERT_EQ(p2.first,  edBC);
    ASSERT_EQ(p2.second, false);

    pair<edge_descriptor, bool> p3 = edge(vdA, vdB, g);
    ASSERT_EQ(p3.first,  edAB);
    ASSERT_EQ(p3.second, true);

    pair<edge_descriptor, bool> p4 = edge(vdB, vdC, g);
    ASSERT_EQ(p4.first,  edBC);
    ASSERT_EQ(p4.second, true);

    pair<edge_descriptor, bool> p5 = edge(vdA, vdC, g);
    ASSERT_EQ(p5.second, false);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 3u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);

    vertex_descriptor vd3 = source(edBC, g);
    ASSERT_EQ(vd3, vdB);

    vertex_descriptor vd4 = target(edBC, g);
    ASSERT_EQ(vd4, vdC);
}

TYPED_TEST(GraphFixture, test7) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, vdC);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test8) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edBC);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edCA);
    ++b;
    ASSERT_EQ(e, b);
}

//adjacent_vertices tests
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdD, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, vdD);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd3 = *b2;
    ASSERT_EQ(vd3, vdC);
    ++b2;
    ASSERT_EQ(e2, b2);
}

TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd2 = *b2;
    ASSERT_EQ(vd2, vdC);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdC, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd3 = *b3;
    ASSERT_EQ(vd3, vdA);
    ++b3;
    ASSERT_EQ(e3, b3);
}

TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdE, g);
    add_edge(vdA, vdD, g);
    add_edge(vdC, vdB, g);
    add_edge(vdC, vdE, g);
    add_edge(vdC, vdD, g);
    add_edge(vdE, vdB, g);
    add_edge(vdE, vdD, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdD);
    ++b;

    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, vdE);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdC, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd4 = *b2;
    ASSERT_EQ(vd4, vdB);
    ++b2;

    vertex_descriptor vd5 = *b2;
    ASSERT_EQ(vd5, vdD);
    ++b2;

    vertex_descriptor vd6 = *b2;
    ASSERT_EQ(vd6, vdE);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdE, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd7 = *b3;
    ASSERT_EQ(vd7, vdB);
    ++b3;

    vertex_descriptor vd8 = *b3;
    ASSERT_EQ(vd8, vdD);
    ++b3;
    ASSERT_EQ(e3, b3);
}

TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdD, g);
    add_edge(vdA, vdE, g);
    add_edge(vdB, vdA, g);
    add_edge(vdB, vdE, g);
    add_edge(vdC, vdB, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdE, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdD);
    ++b;

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdE);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd3 = *b2;
    ASSERT_EQ(vd3, vdA);
    ++b2;

    vertex_descriptor vd4 = *b2;
    ASSERT_EQ(vd4, vdE);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdC, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd5 = *b3;
    ASSERT_EQ(vd5, vdB);
    ++b3;

    vertex_descriptor vd6 = *b3;
    ASSERT_EQ(vd6, vdE);
    ++b3;
    ASSERT_EQ(e3, b3);

    pair<adjacency_iterator, adjacency_iterator> p4 = adjacent_vertices(vdD, g);
    adjacency_iterator                           b4 = p4.first;
    adjacency_iterator                           e4 = p4.second;
    ASSERT_NE(b4, e4);

    vertex_descriptor vd7 = *b4;
    ASSERT_EQ(vd7, vdC);
    ++b4;

    vertex_descriptor vd8 = *b4;
    ASSERT_EQ(vd8, vdE);
    ++b4;
    ASSERT_EQ(e4, b4);

    pair<adjacency_iterator, adjacency_iterator> p5 = adjacent_vertices(vdE, g);
    adjacency_iterator                           b5 = p5.first;
    adjacency_iterator                           e5 = p5.second;
    ASSERT_EQ(e5, b5);
}

TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdE, vdA, g);
    add_edge(vdE, vdB, g);
    add_edge(vdE, vdC, g);
    add_edge(vdE, vdD, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd2 = *b2;
    ASSERT_EQ(vd2, vdC);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdC, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd3 = *b3;
    ASSERT_EQ(vd3, vdD);
    ++b3;
    ASSERT_EQ(e3, b3);

    pair<adjacency_iterator, adjacency_iterator> p4 = adjacent_vertices(vdD, g);
    adjacency_iterator                           b4 = p4.first;
    adjacency_iterator                           e4 = p4.second;
    ASSERT_NE(b4, e4);

    vertex_descriptor vd4 = *b4;
    ASSERT_EQ(vd4, vdA);
    ++b4;
    ASSERT_EQ(e4, b4);

    pair<adjacency_iterator, adjacency_iterator> p5 = adjacent_vertices(vdE, g);
    adjacency_iterator                           b5 = p5.first;
    adjacency_iterator                           e5 = p5.second;
    ASSERT_NE(b5, e5);

    vertex_descriptor vd5 = *b5;
    ASSERT_EQ(vd5, vdA);
    ++b5;

    vertex_descriptor vd6 = *b5;
    ASSERT_EQ(vd6, vdB);
    ++b5;

    vertex_descriptor vd7 = *b5;
    ASSERT_EQ(vd7, vdC);
    ++b5;

    vertex_descriptor vd8 = *b5;
    ASSERT_EQ(vd8, vdD);
    ++b5;
    ASSERT_EQ(e5, b5);
}

TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);
    vertex_descriptor vdI = add_vertex(g);
    vertex_descriptor vdJ = add_vertex(g);
    vertex_descriptor vdK = add_vertex(g);
    vertex_descriptor vdL = add_vertex(g);
    vertex_descriptor vdM = add_vertex(g);
    vertex_descriptor vdN = add_vertex(g);
    vertex_descriptor vdO = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdD, vdC, g);
    add_edge(vdE, vdI, g);
    add_edge(vdF, vdB, g);
    add_edge(vdF, vdG, g);
    add_edge(vdF, vdL, g);
    add_edge(vdG, vdA, g);
    add_edge(vdG, vdC, g);
    add_edge(vdH, vdC, g);
    add_edge(vdH, vdD, g);
    add_edge(vdI, vdN, g);
    add_edge(vdJ, vdK, g);
    add_edge(vdJ, vdM, g);
    add_edge(vdJ, vdN, g);
    add_edge(vdK, vdH, g);
    add_edge(vdK, vdL, g);
    add_edge(vdL, vdH, g);
    add_edge(vdM, vdI, g);
    add_edge(vdN, vdO, g);
    add_edge(vdO, vdK, g);
    add_edge(vdO, vdL, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdD, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd2 = *b2;
    ASSERT_EQ(vd2, vdC);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdE, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd3 = *b3;
    ASSERT_EQ(vd3, vdI);
    ++b3;
    ASSERT_EQ(e3, b3);

    pair<adjacency_iterator, adjacency_iterator> p4 = adjacent_vertices(vdF, g);
    adjacency_iterator                           b4 = p4.first;
    adjacency_iterator                           e4 = p4.second;
    ASSERT_NE(b4, e4);

    vertex_descriptor vd4 = *b4;
    ASSERT_EQ(vd4, vdB);
    ++b4;

    vertex_descriptor vd5 = *b4;
    ASSERT_EQ(vd5, vdG);
    ++b4;

    vertex_descriptor vd6 = *b4;
    ASSERT_EQ(vd6, vdL);
    ++b4;
    ASSERT_EQ(e4, b4);

    pair<adjacency_iterator, adjacency_iterator> p5 = adjacent_vertices(vdG, g);
    adjacency_iterator                           b5 = p5.first;
    adjacency_iterator                           e5 = p5.second;
    ASSERT_NE(b5, e5);

    vertex_descriptor vd7 = *b5;
    ASSERT_EQ(vd7, vdA);
    ++b5;

    vertex_descriptor vd8 = *b5;
    ASSERT_EQ(vd8, vdC);
    ++b5;
    ASSERT_EQ(e5, b5);

    pair<adjacency_iterator, adjacency_iterator> p6 = adjacent_vertices(vdJ, g);
    adjacency_iterator                           b6 = p6.first;
    adjacency_iterator                           e6 = p6.second;
    ASSERT_NE(b6, e6);

    vertex_descriptor vd9 = *b6;
    ASSERT_EQ(vd9, vdK);
    ++b6;

    vertex_descriptor vd10 = *b6;
    ASSERT_EQ(vd10, vdM);
    ++b6;

    vertex_descriptor vd11 = *b6;
    ASSERT_EQ(vd11, vdN);
    ++b6;
    ASSERT_EQ(e6, b6);

    pair<adjacency_iterator, adjacency_iterator> p7 = adjacent_vertices(vdO, g);
    adjacency_iterator                           b7 = p7.first;
    adjacency_iterator                           e7 = p7.second;
    ASSERT_NE(b7, e7);

    vertex_descriptor vd12 = *b7;
    ASSERT_EQ(vd12, vdK);
    ++b7;

    vertex_descriptor vd13 = *b7;
    ASSERT_EQ(vd13, vdL);
    ++b7;
    ASSERT_EQ(e7, b7);
}

//num_vertices and num_edges tests
TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);

    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 3);
}

TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);

    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 2);
}

TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 2);
}

TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);

    int count = 0;
    while( count < 1000) {
        add_edge(vdA, vdC, g);
        count += 20;
    }

    ASSERT_EQ(num_vertices(g), 3);
    ASSERT_EQ(num_edges(g), 3);
}

//source and target tests
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    vertex_descriptor vd1 = source(edCB, g);
    ASSERT_EQ(vd1, vdC);

    vertex_descriptor vd2 = target(edCB, g);
    ASSERT_EQ(vd2, vdB);

    vertex_descriptor vd3 = source(edBA, g);
    ASSERT_EQ(vd3, vdB);

    vertex_descriptor vd4 = target(edBA, g);
    ASSERT_EQ(vd4, vdA);

    vertex_descriptor vd5 = source(edAC, g);
    ASSERT_EQ(vd5, vdA);

    vertex_descriptor vd6 = target(edAC, g);
    ASSERT_EQ(vd6, vdC);
}

TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAE = add_edge(vdA, vdE, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;
    edge_descriptor edEB = add_edge(vdE, vdB, g).first;
    edge_descriptor edED = add_edge(vdE, vdD, g).first;

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);

    vertex_descriptor vd3 = source(edAE, g);
    ASSERT_EQ(vd3, vdA);

    vertex_descriptor vd4 = target(edAE, g);
    ASSERT_EQ(vd4, vdE);

    vertex_descriptor vd5 = source(edAD, g);
    ASSERT_EQ(vd5, vdA);

    vertex_descriptor vd6 = target(edAD, g);
    ASSERT_EQ(vd6, vdD);

    vertex_descriptor vd7 = source(edEB, g);
    ASSERT_EQ(vd7, vdE);

    vertex_descriptor vd8 = target(edEB, g);
    ASSERT_EQ(vd8, vdB);

    vertex_descriptor vd9 = source(edED, g);
    ASSERT_EQ(vd9, vdE);

    vertex_descriptor vd10 = target(edED, g);
    ASSERT_EQ(vd10, vdD);
}

TYPED_TEST(GraphFixture, test24) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,2};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

//DFS tests
TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,3,2,4};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,3,2,1,4};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdD, g);
    add_edge(vdA, vdE, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list <boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits <graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,4,5,2,6,3,8,9,7,10};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);
    vertex_descriptor vdK = add_vertex(g);
    vertex_descriptor vdL = add_vertex(g);
    vertex_descriptor vdO = add_vertex(g);
    vertex_descriptor vdP = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdG, g);
    add_edge(vdG, vdC, g);
    add_edge(vdC, vdH, g);
    add_edge(vdH, vdD, g);
    add_edge(vdH, vdL, g);
    add_edge(vdL, vdO, g);
    add_edge(vdL, vdP, g);
    add_edge(vdO, vdK, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdA, vdB, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdB, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd3 = *b2;
    ASSERT_EQ(vd3, vdB);
    ++b2;
    ASSERT_EQ(b2, e2);
}

TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdB, g);
    add_edge(vdC, vdA, g);
    add_edge(vdC, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(e, b);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd3 = *b2;
    ASSERT_EQ(vd3, vdB);
    ++b2;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd4 = *b2;
    ASSERT_EQ(vd4, vdC);
    ++b2;
    ASSERT_EQ(e2, b2);

    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdC, g);
    adjacency_iterator                           b3 = p3.first;
    adjacency_iterator                           e3 = p3.second;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd5 = *b3;
    ASSERT_EQ(vd5, vdA);
    ++b3;
    ASSERT_NE(b3, e3);

    vertex_descriptor vd6 = *b3;
    ASSERT_EQ(vd6, vdC);
    ++b3;
    ASSERT_EQ(e3, b3);
}

//BFS tests
TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    using Size = graph_traits < graph_t >::vertices_size_type;
    using Iiter = Size*;

    int expected_BFS[] = {0,1,2};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_BFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    using Size = graph_traits < graph_t >::vertices_size_type;
    using Iiter = Size*;

    int expected_BFS[] = {0,1,2,3,4};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_BFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    using Size = graph_traits < graph_t >::vertices_size_type;
    using Iiter = Size*;

    int expected_BFS[] = {0,3,4,2,1};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdD, g);
    add_edge(vdA, vdE, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_BFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    using Size = graph_traits < graph_t >::vertices_size_type;
    using Iiter = Size*;

    int expected_BFS[] = {0,1,4,5,2,6,3,8,9,10,7};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);
    vertex_descriptor vdK = add_vertex(g);
    vertex_descriptor vdL = add_vertex(g);
    vertex_descriptor vdO = add_vertex(g);
    vertex_descriptor vdP = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdG, g);
    add_edge(vdG, vdC, g);
    add_edge(vdC, vdH, g);
    add_edge(vdH, vdD, g);
    add_edge(vdH, vdL, g);
    add_edge(vdL, vdO, g);
    add_edge(vdL, vdP, g);
    add_edge(vdO, vdK, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_BFS[i], discover_order[i] );
    }
}

//topological order tests
TYPED_TEST(GraphFixture, test35) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    int expected_TO[] = {0,2,1};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    using container = std::vector< Vertex >;
    container c;
    topological_sort(g, std::back_inserter(c));

    int i = 0;
    for ( container::reverse_iterator ii=c.rbegin(); ii!=c.rend(); ++ii) {
        ASSERT_EQ(expected_TO[i], *ii);
        ++i;
    }
}

TYPED_TEST(GraphFixture, test36) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    int expected_TO[] = {0,2,4,1,3};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);

    using container = std::vector< Vertex >;
    container c;
    topological_sort(g, std::back_inserter(c));

    int i = 0;
    for ( container::reverse_iterator ii=c.rbegin(); ii!=c.rend(); ++ii) {
        ASSERT_EQ(expected_TO[i], *ii);
        ++i;
    }
}

//component tests
TYPED_TEST(GraphFixture, test37) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));
    ASSERT_EQ(num, 1);
    ASSERT_EQ(component[0], 0);
    ASSERT_EQ(component[1], 0);
    ASSERT_EQ(component[2], 0);
}

TYPED_TEST(GraphFixture, test38) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdC, g);

    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));

    ASSERT_EQ(num, 1);
    ASSERT_EQ(component[0], component[1]);
    ASSERT_EQ(component[1], component[2]);
    ASSERT_EQ(component[2], component[0]);
}

TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdD, g);
    add_edge(vdA, vdE, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);

    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));
    ASSERT_EQ(num, 5);
    for(vertices_size_type i = 0; i<num_vertices(g); ++i) {
        for(vertices_size_type j = 0; j<num_vertices(g); ++j) {
            if(i != j) {
                ASSERT_NE(component[i], component[j]);
            }
        }
    }
}

TYPED_TEST(GraphFixture, test40) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);
    vertex_descriptor vdK = add_vertex(g);
    vertex_descriptor vdL = add_vertex(g);
    vertex_descriptor vdO = add_vertex(g);
    vertex_descriptor vdP = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdG, g);
    add_edge(vdG, vdC, g);
    add_edge(vdC, vdH, g);
    add_edge(vdH, vdD, g);
    add_edge(vdH, vdL, g);
    add_edge(vdL, vdO, g);
    add_edge(vdL, vdP, g);
    add_edge(vdO, vdK, g);
    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));
    ASSERT_EQ(num, 11);
}

TYPED_TEST(GraphFixture, test41) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using Vertex = graph_traits < graph_t >::vertex_descriptor;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdF, vdA, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdH, g);
    add_edge(vdH, vdF, g);
    add_edge(vdH, vdG, g);

    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));
    ASSERT_EQ(num, 4);
    for(vertices_size_type i = 1; i<5; ++i) {
        for(vertices_size_type j = 1; j<5; ++j) {
            ASSERT_EQ(component[i], component[j]);
        }
    }
    ASSERT_EQ(component[6], component[7]);
    ASSERT_NE(component[1], component[6]);
    ASSERT_NE(component[1], component[0]);
    ASSERT_NE(component[1], component[5]);
    ASSERT_NE(component[5], component[0]);
}

TYPED_TEST(GraphFixture, test42) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,2,4,3,5,6,7};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdF, vdA, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdH, g);
    add_edge(vdH, vdF, g);
    add_edge(vdH, vdG, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test43) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    typedef graph_traits < graph_t >::vertices_size_type Size;
    typedef Size* Iiter;

    int expected_BFS[] = {0,5,6,7,1,3,2,4};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdF, vdA, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdH, g);
    add_edge(vdH, vdF, g);
    add_edge(vdH, vdG, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    //check with expected order
    for (vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_BFS[i], discover_order[i] );
    }
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 0) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 0) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 0) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 5) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 5) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 5) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 6) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 6) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 6) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 2) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 4) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 2) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 4) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 2) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 4) - discover_order.begin());
}

TYPED_TEST(GraphFixture, test44) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,2,4,3,5,6,7};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdF, vdA, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdH, g);
    add_edge(vdH, vdF, g);
    add_edge(vdH, vdG, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for (size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test45) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdF, g);
    add_edge(vdF, vdD, g);

    std::vector<int> component(num_vertices(g));
    int num = connected_components(g, &component[0]);

    ASSERT_EQ(num, 2);
    ASSERT_EQ(component[0], component[1]);
    ASSERT_EQ(component[1], component[2]);
    ASSERT_EQ(component[2], component[0]);
    ASSERT_EQ(component[3], component[4]);
    ASSERT_EQ(component[4], component[5]);
    ASSERT_EQ(component[5], component[3]);
}

TYPED_TEST(GraphFixture, test46) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdD, g);
    add_edge(vdB, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdC, vdE, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdF, vdA, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdH, g);
    add_edge(vdH, vdF, g);
    add_edge(vdH, vdG, g);

    std::vector<int> component(num_vertices(g));
    int num = connected_components(g, &component[0]);

    ASSERT_EQ(num, 3);
    ASSERT_EQ(component[0], component[1]);
    ASSERT_EQ(component[1], component[2]);
    ASSERT_EQ(component[2], component[0]);
    ASSERT_EQ(component[3], component[4]);
    ASSERT_EQ(component[0], component[4]);
    ASSERT_EQ(component[6], component[7]);
    ASSERT_NE(component[5], component[0]);
    ASSERT_NE(component[5], component[6]);
}

TYPED_TEST(GraphFixture, test47) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdC, vdA, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);
    add_edge(vdE, vdD, g);
    add_edge(vdE, vdC, g);
    add_edge(vdF, vdE, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdE, g);
    add_edge(vdH, vdG, g);
    add_edge(vdH, vdF, g);

    std::vector<int> component(num_vertices(g));
    int num = connected_components(g, &component[0]);

    ASSERT_EQ(num, 8);

    for(vertices_size_type i = 1; i<num_vertices(g); ++i) {
        for(vertices_size_type j = 1; j<num_vertices(g); ++j) {
            if(i != j) {
                ASSERT_NE(component[i], component[j]);
            }
        }
    }
}

TYPED_TEST(GraphFixture, test48) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    typedef graph_traits < graph_t >::vertex_descriptor Vertex;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdC, vdA, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);
    add_edge(vdE, vdD, g);
    add_edge(vdE, vdC, g);
    add_edge(vdF, vdE, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdE, g);
    add_edge(vdH, vdG, g);
    add_edge(vdH, vdF, g);

    std::vector< int > component(num_vertices(g)),
        discover_time(num_vertices(g));
    std::vector< default_color_type > color(num_vertices(g));
    std::vector< Vertex > root(num_vertices(g));

    int num = strong_components(g,
                                make_iterator_property_map(component.begin(), get(vertex_index, g)),
                                root_map(make_iterator_property_map(root.begin(), get(vertex_index, g)))
                                .color_map(
                                    make_iterator_property_map(color.begin(), get(vertex_index, g)))
                                .discover_time_map(make_iterator_property_map(
                                            discover_time.begin(), get(vertex_index, g))));

    ASSERT_EQ(num, 8);

    for(vertices_size_type i = 1; i<num_vertices(g); ++i) {
        for(vertices_size_type j = 1; j<num_vertices(g); ++j) {
            if(i != j) {
                ASSERT_NE(component[i], component[j]);
            }
        }
    }
}

TYPED_TEST(GraphFixture, test49) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type  = typename graph_type::vertices_size_type;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;
    using size_type = typename boost::graph_traits < graph_t >::vertices_size_type;

    int expected_DFS[] = {0,1,2,3,4,5,6,7};
    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdC, vdA, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);
    add_edge(vdE, vdD, g);
    add_edge(vdE, vdC, g);
    add_edge(vdF, vdE, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdE, g);
    add_edge(vdH, vdG, g);
    add_edge(vdH, vdF, g);

    std::vector < size_type > dtime(num_vertices(g));
    std::vector < size_type > ftime(num_vertices(g));

    using time_pm_type =
        boost::iterator_property_map<std::vector<size_type>::iterator,
        boost::property_map<graph_t, boost::vertex_index_t>::const_type>;

    time_pm_type dtime_pm(dtime.begin(), get(boost::vertex_index, g));
    time_pm_type ftime_pm(ftime.begin(), get(boost::vertex_index, g));
    size_type t = 0;
    dfs_time_visitor < time_pm_type >vis(dtime_pm, ftime_pm, t);

    depth_first_search(g, visitor(vis));

    std::vector < size_type > discover_order(num_vertices(g));
    integer_range < size_type > r(0, num_vertices(g));
    std::copy(r.begin(), r.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < time_pm_type, std::less < size_type > >(dtime_pm));

    //check with expected order
    for(vertices_size_type i = 0; i < num_vertices(g); ++i) {
        ASSERT_EQ(expected_DFS[i], discover_order[i] );
    }
}

TYPED_TEST(GraphFixture, test50) {
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    using graph_t  = typename boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS >;

    typedef graph_traits < graph_t >::vertices_size_type Size;
    typedef Size* Iiter;

    graph_t g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);

    add_edge(vdC, vdB, g);
    add_edge(vdC, vdA, g);
    add_edge(vdD, vdC, g);
    add_edge(vdD, vdB, g);
    add_edge(vdE, vdD, g);
    add_edge(vdE, vdC, g);
    add_edge(vdF, vdE, g);
    add_edge(vdF, vdD, g);
    add_edge(vdG, vdF, g);
    add_edge(vdG, vdE, g);
    add_edge(vdH, vdG, g);
    add_edge(vdH, vdF, g);

    std::vector < Size > dtime(num_vertices(g));

    Size time = 0;
    bfs_time_visitor < Size * >vis(&dtime[0], time);
    breadth_first_search(g, vertex(0, g), visitor(vis));

    std::vector<graph_traits<graph_t>::vertices_size_type > discover_order(num_vertices(g));
    integer_range < int >range(0, num_vertices(g));
    std::copy(range.begin(), range.end(), discover_order.begin());
    std::sort(discover_order.begin(), discover_order.end(),
              indirect_cmp < Iiter, std::less < Size > >(&dtime[0]));

    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 0) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 1) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 2) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 2) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 3) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 4) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 4) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 5) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 5) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 6) - discover_order.begin());
    ASSERT_LE(find(discover_order.begin(),discover_order.end(), 6) - discover_order.begin(), find(discover_order.begin(),discover_order.end(), 7) - discover_order.begin());
}

// random graph between 50 to 249 vertices
TYPED_TEST(GraphFixture, test51) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    srand(time(NULL));
    int rand_size =  rand() % 200 + 50;

    for (int i = 0; i < rand_size; ++i) {
        add_vertex(g);
    }

    for (int i = 0; i < rand_size; ++i) {
        for (int j = i; j < rand_size; ++j) {
            add_edge(i, j, g);
        }
    }

    ASSERT_EQ(num_vertices(g), rand_size);
    ASSERT_EQ(num_edges(g), ((rand_size) * (rand_size + 1) / 2 ));
}