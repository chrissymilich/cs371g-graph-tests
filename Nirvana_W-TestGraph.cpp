// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
         Graph // uncomment, add a comma to the line above
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// ------------
// Vertex, 6 tests
// ------------

TYPED_TEST(GraphFixture, vertex0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, vertex1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor v1 = vertex(10000, g);
    vertex_descriptor v2 = vertex(0, g);
    ASSERT_TRUE(v1 != v2);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 0u);}

TYPED_TEST(GraphFixture, vertex2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    ASSERT_TRUE(v1 != v2);
    
    vertex_descriptor vd1 = vertex(0,g);
    vertex_descriptor vd2 = vertex(1,g);
    ASSERT_TRUE(vd1 != vd2);
    ASSERT_EQ(v1,vd1);
    ASSERT_EQ(v2,vd2);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2u);}

TYPED_TEST(GraphFixture, vertex3) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    ASSERT_EQ(*b,v1);
    b++;
    ASSERT_EQ(*b,v2);
    b++;
    ASSERT_EQ(*b,v3);
    b++;
    ASSERT_EQ(b,e);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 3u);}

TYPED_TEST(GraphFixture, vertex4) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    graph_type g;
    graph_type g2;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v21 = add_vertex(g2);
    vertex_descriptor v22 = add_vertex(g2);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    ASSERT_EQ(*b,v1);
    b++;
    ASSERT_EQ(*b,v2);
    b++;
    ASSERT_EQ(b,e);

    pair<vertex_iterator, vertex_iterator> p2 = vertices(g);
    vertex_iterator                        b2 = p2.first;
    vertex_iterator                        e2 = p2.second;

    ASSERT_EQ(*b2,v21);
    b2++;
    ASSERT_EQ(*b2,v22);
    b2++;
    ASSERT_EQ(b2,e2);
}

TYPED_TEST(GraphFixture, vertex5) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    graph_type g;
    graph_type g2;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g2);

    vertex_descriptor vd1 = vertex(0,g);
    vertex_descriptor vd2 = vertex(0,g2);
    ASSERT_EQ(v1,v2);
    ASSERT_EQ(vd1,vd2);}

// ------------
// Edge, 16 tests
// ------------

TYPED_TEST(GraphFixture, edge0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, edge1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdA, g);
    ASSERT_EQ(p1.first,  edAA);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdA, g);
    ASSERT_EQ(p2.first,  edAA);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAA, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAA, g);
    ASSERT_EQ(vd2, vdA);}

TYPED_TEST(GraphFixture, edge2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);
    vertex_descriptor vdc = add_vertex(g);

    edge_descriptor edab = add_edge(vda, vdb, g).first;
    edge_descriptor edac = add_edge(vda, vdc, g).first;

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);

    ASSERT_EQ(source(edab, g), source(edac, g));

    ASSERT_EQ(target(edab,g), vdb);
    ASSERT_EQ(target(edac,g), vdc);
    }

TYPED_TEST(GraphFixture, edge3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);

    edge_descriptor edab = add_edge(vda, vdb, g).first;
    edge_descriptor edba = add_edge(vdb, vda, g).first;

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);

    ASSERT_EQ(source(edab, g), target(edba, g));
}

TYPED_TEST(GraphFixture, edge4) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);

    edge_descriptor edab = add_edge(vda, vdb, g).first;
    edge_descriptor edba = add_edge(vdb, vda, g).first;

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);

    ASSERT_EQ(edge(vda, vdb, g).first, edab);
    ASSERT_EQ(edge(vdb, vda, g).first, edba);
}

TYPED_TEST(GraphFixture, edge5) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);

    ASSERT_EQ(edge(vda, vdb, g).second, false);

    ASSERT_EQ(add_edge(vda, vdb, g).second,true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

TYPED_TEST(GraphFixture, edge6) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);

    add_edge(vda, vdb, g);
    ASSERT_EQ(edge(vda, vdb, g).second, true);
    ASSERT_EQ(add_edge(vda, vdb, g).second, false);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

TYPED_TEST(GraphFixture, edge7) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBA);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, edge8) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAA = add_edge(vdA, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAA);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, edge9) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCC = add_edge(vdC, vdC, g).first;
    edge_descriptor edBB = add_edge(vdB, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edBA);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edBB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edCC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, edge10) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCC = add_edge(vdC, vdC, g).first;
    edge_descriptor edBB = add_edge(vdB, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edBA);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edBB);
    ++b;
    ASSERT_NE(b, e);

    add_edge(vdB, vdC, g);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edCC);
    ++b;}

TYPED_TEST(GraphFixture, edge11) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edBA);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edCA);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, edge12) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdC, vdA, g);
    add_edge(vdA, vdA, g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdA, g);

    ASSERT_EQ(num_edges(g),5u);}

TYPED_TEST(GraphFixture, edge13) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdC, vdA, g);
    add_edge(vdA, vdA, g);
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdA, g);
    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edge(vdA,vdA,g).first);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edge(vdA,vdB,g).first);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edge(vdA,vdC,g).first);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed4 = *b;
    ASSERT_EQ(ed4, edge(vdB,vdA,g).first);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed5 = *b;
    ASSERT_EQ(ed5, edge(vdC,vdA,g).first);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edge14) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor ed = add_edge(vdC, vdA, g).first;

    ASSERT_EQ(add_edge(vdC, vdA, g).second,false);
    ASSERT_EQ(add_edge(vdC, vdA, g).first,ed);}

TYPED_TEST(GraphFixture, edge15) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB1 = add_vertex(g1);
    vertex_descriptor vdB2 = add_vertex(g2);

    add_edge(vdA1, vdB1, g1);
    add_edge(vdB2, vdA2, g2);
    pair<edge_iterator, edge_iterator> p1 = edges(g1);
    edge_iterator                      b1 = p1.first;

    pair<edge_iterator, edge_iterator> p2 = edges(g2);
    edge_iterator                      b2 = p2.first;

    edge_descriptor ed11 = *b1;
    ASSERT_EQ(ed11, edge(vdA1,vdB1,g1).first);
    ++b1;

    edge_descriptor ed21 = *b2;
    ASSERT_EQ(ed21, edge(vdB2,vdA2,g2).first);
    ++b2;
}

//------------
// Adj, 5 tests
//------------

TYPED_TEST(GraphFixture, adj0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, adj1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, adj2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, vdC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, adj3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdB, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, adj4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdB2 = add_vertex(g2);

    add_edge(vdA, vdB, g);
    add_edge(vdA2, vdB2, g2);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(b, e);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdA2, g2);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd21 = *b2;
    ASSERT_EQ(vd21, vdB2);
    ++b2;
    ASSERT_EQ(b2, e2);
}

//------------
// source & target, 8 tests
//------------
TYPED_TEST(GraphFixture, source0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    ASSERT_EQ(source(edAB,g),vdA);}

TYPED_TEST(GraphFixture, source1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(source(edAA,g),vdA);}

TYPED_TEST(GraphFixture, source2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    ASSERT_EQ(source(edBA,g),vdB);}

TYPED_TEST(GraphFixture, source3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdB2 = add_vertex(g2);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBA2 = add_edge(vdB2, vdA2, g2).first;

    ASSERT_EQ(source(edBA,g),vdB);
    ASSERT_EQ(source(edBA2,g),vdB2);}

TYPED_TEST(GraphFixture, target0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    ASSERT_EQ(target(edAB,g),vdB);}

TYPED_TEST(GraphFixture, target1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(target(edAA,g),vdA);}

TYPED_TEST(GraphFixture, target2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    ASSERT_EQ(target(edBA,g),vdA);}

TYPED_TEST(GraphFixture, target3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdA2 = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdB2 = add_vertex(g2);

    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBA2 = add_edge(vdB2, vdA2, g2).first;

    ASSERT_EQ(target(edBA,g),vdA);
    ASSERT_EQ(target(edBA2,g),vdA2);}

//------------
// num, 6 tests
//------------
TYPED_TEST(GraphFixture, num_vertices0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;
    add_vertex(g);
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, num_vertices1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 4u);}

TYPED_TEST(GraphFixture, num_vertices2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;
    add_vertex(g);
    add_vertex(g);

    graph_type g2;
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2u);

    vertices_size_type vs2 = num_vertices(g2);
    ASSERT_EQ(vs2, 3u);
}

TYPED_TEST(GraphFixture, num_edges0) {
    using graph_type        = typename TestFixture::graph_type;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 0u);
}

TYPED_TEST(GraphFixture, num_edges1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vdb = add_vertex(g);

    add_edge(vda, vdb, g);
    add_edge(vdb, vda, g);
    add_edge(vda, vda, g);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 3u);

    add_edge(vda, vda, g);
    edges_size_type es2 = num_edges(g);
    ASSERT_EQ(es2, 3u);
}

TYPED_TEST(GraphFixture, num_edges2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;
    graph_type g2;

    vertex_descriptor vda = add_vertex(g);
    vertex_descriptor vda2 = add_vertex(g2);
    vertex_descriptor vdb = add_vertex(g);
    vertex_descriptor vdb2 = add_vertex(g2);

    add_edge(vda, vdb, g);
    add_edge(vda2, vdb2, g2);
    add_edge(vda2, vda2, g2);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
    
    edges_size_type es2 = num_edges(g2);
    ASSERT_EQ(es2, 2u);
}