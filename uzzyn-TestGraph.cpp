// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#define NDEBUG
#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g); // only 2 adjacent vertices here
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// --------
// my_test
// --------
//testing copy constructor
TYPED_TEST(GraphFixture, myTest1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);

    graph_type g2(g);
    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g2);
    ASSERT_EQ(p2.second, true);

    vertex_descriptor vdA2 = vertex(vdA, g2);
    vertex_descriptor vdB2 = vertex(vdB, g2);
    vertex_descriptor vdC2 = vertex(vdC, g2);
    ASSERT_EQ(vdA, vdA2);
    ASSERT_EQ(vdB, vdB2);
    ASSERT_EQ(vdC, vdC2);

    vertices_size_type vs = num_vertices(g2);
    ASSERT_EQ(vs, 3u);
    edges_size_type es = num_edges(g2);
    ASSERT_EQ(es, 1u);
}

//Testing add_vertex, add_edge, and num_vertices
TYPED_TEST(GraphFixture, myTest2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor t = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor s = add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    pair<edge_descriptor, bool> ed = add_edge(t, s, g);
    pair<edge_descriptor, bool> ed2 = add_edge(s, t, g);
    pair<edge_descriptor, bool> ed3 = add_edge(t, s, g);
    ASSERT_EQ(ed.second, true);
    ASSERT_EQ(ed2.second, true);
    ASSERT_EQ(ed3.second, false);
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 10u);
}

//Testing adjacent_vertices()
TYPED_TEST(GraphFixture, myTest3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v1, g);
    add_edge(v3, v1, g);

    pair<adjacency_iterator, adjacency_iterator> ad_v1 = adjacent_vertices(v1, g);
    adjacency_iterator                           b = ad_v1.first;
    adjacency_iterator                           e = ad_v1.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, v2);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, myTest4) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v1, g);
    add_edge(v2, v3, g);
    add_edge(v3, v1, g);

    pair<adjacency_iterator, adjacency_iterator> ad_v2 = adjacent_vertices(v2, g);
    adjacency_iterator                           b = ad_v2.first;
    adjacency_iterator                           e = ad_v2.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, v1);

    ++b;
    ASSERT_NE(b, e);
    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, v3);

    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, myTest5) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v3, g);
    add_edge(v2, v1, g);
    add_edge(v1, v3, g);

    pair<adjacency_iterator, adjacency_iterator> ad_v3 = adjacent_vertices(v3, g);
    adjacency_iterator                           b = ad_v3.first;
    adjacency_iterator                           e = ad_v3.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, myTest6) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    add_edge(v2, v1, g);
    add_edge(v3, v1, g);

    pair<adjacency_iterator, adjacency_iterator> ad_v1 = adjacent_vertices(v1, g);
    adjacency_iterator                           b = ad_v1.first;
    adjacency_iterator                           e = ad_v1.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, v1);

    ++b;
    ASSERT_NE(b, e);
    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, v2);

    ++b;
    ASSERT_NE(b, e);
    vertex_descriptor vd3 = *b;
    ASSERT_EQ(vd3, v3);

    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, myTest7) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v1, g);

    pair<adjacency_iterator, adjacency_iterator> ad_v1 = adjacent_vertices(v1, g);
    adjacency_iterator                           b = ad_v1.first;
    adjacency_iterator                           e = ad_v1.second;
    ASSERT_NE(b, e);
    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, v1);
    ++b;
    ASSERT_EQ(b, e);

    pair<adjacency_iterator, adjacency_iterator> ad_v2 = adjacent_vertices(v2, g);
    b = ad_v2.first;
    e = ad_v2.second;
    ASSERT_EQ(b,e);

    pair<adjacency_iterator, adjacency_iterator> ad_v3 = adjacent_vertices(v3, g);
    b = ad_v3.first;
    e = ad_v3.second;
    ASSERT_EQ(b,e);
}

//testing num_vertices()
TYPED_TEST(GraphFixture, myTest8) {
    using graph_type        = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 31; ++i) {
        add_vertex(g);
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 31u);
}

TYPED_TEST(GraphFixture, myTest9) {
    using graph_type        = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 123; ++i) {
        add_vertex(g);
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 123u);
}

TYPED_TEST(GraphFixture, myTest10) {
    using graph_type        = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 24; ++i) {
        add_vertex(g);
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 24u);
}

TYPED_TEST(GraphFixture, myTest11) {
    using graph_type        = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 11; ++i) {
        add_vertex(g);
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 11u);
}

//Testing num_edges()
TYPED_TEST(GraphFixture, myTest12) {
    using graph_type        = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v1, v2, g);
    add_edge(v1, v2, g);
    add_edge(v1, v2, g);

    add_edge(v2, v1, g);
    add_edge(v2, v3, g);
    add_edge(v3, v2, g);
    add_edge(v1, v3, g);

    edges_size_type ne = num_edges(g);
    ASSERT_EQ(ne, 5u);
}

TYPED_TEST(GraphFixture, myTest13) {
    using graph_type        = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v1, v2, g);

    add_edge(v2, v1, g);
    add_edge(v2, v1, g);
    add_edge(v2, v1, g);
    add_edge(v2, v1, g);
    add_edge(v2, v1, g);

    add_edge(v2, v3, g);
    add_edge(v2, v3, g);

    add_edge(v3, v2, g);
    add_edge(v1, v3, g);
    add_edge(v3, v1, g);

    edges_size_type ne = num_edges(g);
    ASSERT_EQ(ne, 6u);
}

TYPED_TEST(GraphFixture, myTest14) {
    using graph_type        = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    add_vertex(g);
    vertex_descriptor v6 = add_vertex(g);

    add_edge(v1, v6, g);
    add_edge(v1, v6, g);
    add_edge(v1, v6, g);
    add_edge(v1, v6, g);
    add_edge(v1, v6, g);

    add_edge(v2, v4, g);

    edges_size_type ne = num_edges(g);
    ASSERT_EQ(ne, 2u);
}

TYPED_TEST(GraphFixture, myTest15) {
    using graph_type        = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);
    vertex_descriptor v6 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    add_edge(v1, v4, g);
    add_edge(v1, v5, g);
    add_edge(v1, v6, g);
    add_edge(v1, v6, g);

    edges_size_type ne = num_edges(g);
    ASSERT_EQ(ne, 6u);
}

//Testing target() & source()
TYPED_TEST(GraphFixture, myTest16) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);
    vertex_descriptor v6 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    add_edge(v1, v4, g);
    add_edge(v1, v5, g);
    edge_descriptor ed16 = add_edge(v1, v6, g).first;
    edge_descriptor ed61 = add_edge(v6, v1, g).first;

    vertex_descriptor s1 = source(ed16, g);
    vertex_descriptor s2 = source(ed61, g);
    vertex_descriptor t1 = target(ed16, g);
    vertex_descriptor t2 = target(ed61, g);
    ASSERT_EQ(s1, v1);
    ASSERT_EQ(s2, v6);
    ASSERT_EQ(t1, v6);
    ASSERT_EQ(t2, v1);
}

TYPED_TEST(GraphFixture, myTest17) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);
    vertex_descriptor v6 = add_vertex(g);

    edge_descriptor ed11 = add_edge(v1, v1, g).first;
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    edge_descriptor ed44 = add_edge(v4, v4, g).first;
    edge_descriptor ed45 = add_edge(v4, v5, g).first;
    add_edge(v1, v5, g);
    add_edge(v1, v6, g).first;
    add_edge(v6, v1, g).first;

    vertex_descriptor s1 = source(ed11, g);
    vertex_descriptor s2 = source(ed44, g);
    vertex_descriptor s3 = source(ed45, g);
    vertex_descriptor t1 = target(ed11, g);
    vertex_descriptor t2 = target(ed44, g);
    vertex_descriptor t3 = target(ed45, g);

    ASSERT_EQ(s1, v1);
    ASSERT_EQ(s2, v4);
    ASSERT_EQ(s3, v4);
    ASSERT_EQ(t1, v1);
    ASSERT_EQ(t2, v4);
    ASSERT_EQ(t3, v5);
}

TYPED_TEST(GraphFixture, myTest18) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    edge_descriptor ed23 = add_edge(v2, v3, g).first;
    edge_descriptor ed33 = add_edge(v3, v3, g).first;


    vertex_descriptor s1 = source(ed23, g);
    vertex_descriptor s2 = source(ed33, g);
    vertex_descriptor t1 = target(ed23, g);
    vertex_descriptor t2 = target(ed33, g);
    ASSERT_EQ(s1, v2);
    ASSERT_EQ(s2, v3);
    ASSERT_EQ(t1, v3);
    ASSERT_EQ(t2, v3);
}

TYPED_TEST(GraphFixture, myTest19) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    add_edge(v1, v4, g);
    add_edge(v1, v5, g);

    // can still get an edge although it fails to add an edge
    edge_descriptor ed11 = add_edge(v1, v1, g).first;
    edge_descriptor ed15 = add_edge(v1, v5, g).first;

    vertex_descriptor s1 = source(ed11, g);
    vertex_descriptor s2 = source(ed15, g);
    vertex_descriptor t1 = target(ed11, g);
    vertex_descriptor t2 = target(ed15, g);
    ASSERT_EQ(s1, v1);
    ASSERT_EQ(s2, v1);
    ASSERT_EQ(t1, v1);
    ASSERT_EQ(t2, v5);
}

//Testing vertex() - check if vertex is indexed correctly
TYPED_TEST(GraphFixture, myTest20) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor v0 = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor v8 = add_vertex(g);
    vertex_descriptor v9 = add_vertex(g);

    vertex_descriptor vd = vertex(4, g);
    ASSERT_EQ(vd, v4);
    vd = vertex(5, g);
    ASSERT_EQ(vd, v5);
    vd = vertex(0, g);
    ASSERT_EQ(vd, v0);
    vd = vertex(3, g);
    ASSERT_EQ(vd, v3);
    vd = vertex(8, g);
    ASSERT_EQ(vd, v8);
    vd = vertex(9, g);
    ASSERT_EQ(vd, v9);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 10u);
}

TYPED_TEST(GraphFixture, myTest21) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor v0 = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    vertex_descriptor vd = vertex(4, g);
    ASSERT_EQ(vd, v4);
    vd = vertex(3, g);
    ASSERT_EQ(vd, v3);
    vd = vertex(0, g);
    ASSERT_EQ(vd, v0);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 5u);
}

TYPED_TEST(GraphFixture, myTest22) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor v0 = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    vertex_descriptor vd = vertex(4, g);
    ASSERT_EQ(vd, 4);
    vd = vertex(5, g);
    ASSERT_EQ(vd, 5);
    vd = vertex(0, g);
    ASSERT_EQ(vd, v0);
    vd = vertex(3, g);
    ASSERT_EQ(vd, v3);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 8u);
}

TYPED_TEST(GraphFixture, myTest23) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);


    vertex_descriptor vd = vertex(4, g);
    ASSERT_EQ(vd, 4);
    vd = vertex(5, g);
    ASSERT_EQ(vd, v5);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 6u);
}

//Testing add_edges
TYPED_TEST(GraphFixture, myTest24) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    //source == target
    pair<edge_descriptor, bool> ed = add_edge(v1, v1, g);
    edge_descriptor ed11 = ed.first;
    bool result = ed.second;
    ASSERT_EQ(result, true);

    ed = add_edge(v1, v1, g);
    result = ed.second;
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed11, ed.first);
}

TYPED_TEST(GraphFixture, myTest25) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    pair<edge_descriptor, bool> ed = add_edge(v1, v1, g);
    edge_descriptor ed11 = ed.first;
    bool result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed11, ed.first);

    ed = add_edge(v1, v2, g);
    edge_descriptor ed12 = edge(v1, v2, g).first;
    result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed12, ed.first);

    ed = add_edge(v2, v1, g);
    edge_descriptor ed21 = edge(v2, v1, g).first;
    result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed21, ed.first);

    ed = add_edge(v1, v2, g);
    edge_descriptor ed122 = edge(v1, v2, g).first;
    result = ed.second;
    // no duplicate allowed
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed122, ed.first);
}

TYPED_TEST(GraphFixture, myTest26) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    //v2 -> v1
    pair<edge_descriptor, bool> ed = add_edge(v2, v1, g);
    edge_descriptor ed21 = edge(v2, v1, g).first;
    bool result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed21, ed.first);

    //v3 -> v1
    ed = add_edge(v3, v1, g);
    edge_descriptor ed31 = edge(v3, v1, g).first;
    result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed31, ed.first);

    //v1 -> v3 (okay to do this since the graph is directed)
    ed = add_edge(v1, v2, g);
    edge_descriptor ed12 = edge(v1, v2, g).first;
    result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed12, ed.first);

    //v1 -> v3 (okay to do this since the graph is directed)
    ed = add_edge(v1, v3, g);
    edge_descriptor ed13 = edge(v1, v3, g).first;
    result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed13, ed.first);
}

TYPED_TEST(GraphFixture, myTest27) {
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    graph_type g;
    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    pair<edge_descriptor, bool> ed = add_edge(v2, v1, g);
    edge_descriptor ed21 = edge(v2, v1, g).first;
    bool result = ed.second;
    ASSERT_EQ(result, true);
    ASSERT_EQ(ed21, ed.first);

    ed = add_edge(v2, v1, g);
    ed21 = edge(v2, v1, g).first;
    result = ed.second;
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed21, ed.first);

    ed = add_edge(v2, v1, g);
    ed21 = edge(v2, v1, g).first;
    result = ed.second;
    //no duplicate allowed
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed21, ed.first);

    ed = add_edge(v2, v1, g);
    ed21 = edge(v2, v1, g).first;
    result = ed.second;
    //no duplicate allowed
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed21, ed.first);

    ed = add_edge(v2, v1, g);
    ed21 = edge(v2, v1, g).first;
    result = ed.second;
    //no duplicate allowed
    ASSERT_EQ(result, false);
    ASSERT_EQ(ed21, ed.first);
}

//Testing vertices() - check the indexing of each vertex, and num of vertices
TYPED_TEST(GraphFixture, myTest28) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture:: vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 28; ++i) {
        add_vertex(g);
    }
    pair<vertex_iterator, vertex_iterator> v = vertices(g);
    vertex_iterator b = v.first;
    vertex_iterator e = v.second;
    vertex_descriptor counter = 0;
    while(b != e) {
        ASSERT_EQ(*b, counter);
        ++counter;
        ++b;
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, counter);
    ASSERT_EQ(vs, 28u);
}

TYPED_TEST(GraphFixture, myTest29) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture:: vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 78; ++i) {
        add_vertex(g);
    }
    pair<vertex_iterator, vertex_iterator> v = vertices(g);
    vertex_iterator b = v.first;
    vertex_iterator e = v.second;
    vertex_descriptor counter = 0;
    while(b != e) {
        ASSERT_EQ(*b, counter);
        ++counter;
        ++b;
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, counter);
    ASSERT_EQ(vs, 78u);
}

TYPED_TEST(GraphFixture, myTest30) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture:: vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 50; ++i) {
        add_vertex(g);
    }
    pair<vertex_iterator, vertex_iterator> v = vertices(g);
    vertex_iterator b = v.first;
    vertex_iterator e = v.second;
    vertex_descriptor counter = 0;
    while(b != e) {
        ASSERT_EQ(*b, counter);
        ++counter;
        ++b;
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, counter);
    ASSERT_EQ(vs, 50u);
}

TYPED_TEST(GraphFixture, myTest31) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture:: vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 5; ++i) {
        add_vertex(g);
    }
    pair<vertex_iterator, vertex_iterator> v = vertices(g);
    vertex_iterator b = v.first;
    vertex_iterator e = v.second;
    vertex_descriptor counter = 0;
    while(b != e) {
        ASSERT_EQ(*b, counter);
        ++counter;
        ++b;
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, counter);
    ASSERT_EQ(vs, 5u);
}

TYPED_TEST(GraphFixture, myTest32) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture:: vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    graph_type g;
    for(int i = 0; i < 100; ++i) {
        add_vertex(g);
    }
    pair<vertex_iterator, vertex_iterator> v = vertices(g);
    vertex_iterator b = v.first;
    vertex_iterator e = v.second;
    vertex_descriptor counter = 0;
    while(b != e) {
        ASSERT_EQ(*b, counter);
        ++counter;
        ++b;
    }
    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, counter);
    ASSERT_EQ(vs, 100u);
}

//Testing edges()
TYPED_TEST(GraphFixture, myTest33) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    for(int i = 0; i < 5; ++i) {
        add_vertex(g);
    }

    for(int i = 0; i < 5; ++i) {
        add_edge(0, i, g);
    }

    pair<edge_iterator, edge_iterator> eds = edges(g);
    edge_iterator b = eds.first;
    edge_iterator e = eds.second;
    edges_size_type count = 0;
    while(b != e) {
        edge_descriptor current = *b;
        vertex_descriptor src = source(current, g);
        vertex_descriptor tgt = target(current, g);
        ASSERT_EQ(src, 0);
        ASSERT_GE(tgt, 0);
        ASSERT_LT(tgt, 5);
        ++b;
        ++count;
    }
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, count);
    ASSERT_EQ(es, 5u);
}

TYPED_TEST(GraphFixture, myTest34) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    for(int i = 0; i < 5; ++i) {
        add_vertex(g);
    }

    for(int i = 0; i < 3; ++i) {
        add_edge(1, i, g);
    }

    pair<edge_iterator, edge_iterator> eds = edges(g);
    edge_iterator b = eds.first;
    edge_iterator e = eds.second;
    edges_size_type count = 0;
    while(b != e) {
        edge_descriptor current = *b;
        vertex_descriptor src = source(current, g);
        vertex_descriptor tgt = target(current, g);
        ASSERT_EQ(src, 1);
        ASSERT_GE(tgt, 0);
        ASSERT_LT(tgt, 3);
        ++b;
        ++count;
    }
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, count);
    ASSERT_EQ(es, 3u);
}

TYPED_TEST(GraphFixture, myTest35) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    graph_type g;
    for(int i = 0; i < 5; ++i) {
        add_vertex(g);
    }

    for(int i = 0; i < 5; ++i) {
        add_edge(2, 2, g);
    }

    pair<edge_iterator, edge_iterator> eds = edges(g);
    edge_iterator b = eds.first;
    edge_iterator e = eds.second;
    edges_size_type count = 0;
    while(b != e) {
        edge_descriptor current = *b;
        vertex_descriptor src = source(current, g);
        vertex_descriptor tgt = target(current, g);
        ASSERT_EQ(src, 2);
        ASSERT_EQ(tgt, 2);
        ++b;
        ++count;
    }
    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, count);
    ASSERT_EQ(es, 1u);
}


//Testing edge()
TYPED_TEST(GraphFixture, myTest36) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v3, g);
    add_edge(v3, v4, g);
    add_edge(v4, v5, g);
    add_edge(v5, v1, g);

    pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    ASSERT_EQ(e1.second, true);
    pair<edge_descriptor, bool> e2 = edge(v2, v3, g);
    ASSERT_EQ(e2.second, true);
    pair<edge_descriptor, bool> e3 = edge(v3, v4, g);
    ASSERT_EQ(e3.second, true);
    pair<edge_descriptor, bool> e4 = edge(v4, v5, g);
    ASSERT_EQ(e4.second, true);

    e1 = edge(v2, v1, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v3, v2, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v4, v3, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v5, v4, g);
    ASSERT_EQ(e4.second, false);
}

TYPED_TEST(GraphFixture, myTest37) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    add_edge(v1, v1, g);
    add_edge(v2, v2, g);
    add_edge(v3, v3, g);
    add_edge(v4, v4, g);

    pair<edge_descriptor, bool> e1 = edge(v1, v1, g);
    ASSERT_EQ(e1.second, true);
    pair<edge_descriptor, bool> e2 = edge(v2, v2, g);
    ASSERT_EQ(e2.second, true);
    pair<edge_descriptor, bool> e3 = edge(v3, v3, g);
    ASSERT_EQ(e3.second, true);
    pair<edge_descriptor, bool> e4 = edge(v4, v4, g);
    ASSERT_EQ(e4.second, true);

    e1 = edge(v2, v1, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v3, v2, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v4, v3, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v3, v4, g);
    ASSERT_EQ(e4.second, false);
}

TYPED_TEST(GraphFixture, myTest38) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    //v2 and v2 is connected to each other
    //v3 and v4 is connected to each other
    add_edge(v1, v2, g);
    add_edge(v2, v1, g);
    add_edge(v3, v4, g);
    add_edge(v4, v3, g);

    pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    ASSERT_EQ(e1.second, true);
    pair<edge_descriptor, bool> e2 = edge(v2, v1, g);
    ASSERT_EQ(e2.second, true);
    pair<edge_descriptor, bool> e3 = edge(v3, v4, g);
    ASSERT_EQ(e3.second, true);
    pair<edge_descriptor, bool> e4 = edge(v4, v3, g);
    ASSERT_EQ(e4.second, true);

    e1 = edge(v1, v3, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v3, v1, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v2, v3, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v3, v2, g);
    ASSERT_EQ(e4.second, false);
    e1 = edge(v1, v4, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v4, v1, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v2, v4, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v4, v2, g);
    ASSERT_EQ(e4.second, false);
}

TYPED_TEST(GraphFixture, myTest39) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    //v4 has no adjacent edge except itself
    add_edge(v2, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v3, g);
    add_edge(v2, v3, g);
    add_edge(v3, v1, g);
    add_edge(v3, v2, g);
    add_edge(v4, v4, g);

    pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    ASSERT_EQ(e1.second, true);
    pair<edge_descriptor, bool> e2 = edge(v2, v1, g);
    ASSERT_EQ(e2.second, true);
    pair<edge_descriptor, bool> e3 = edge(v3, v1, g);
    ASSERT_EQ(e3.second, true);
    pair<edge_descriptor, bool> e4 = edge(v4, v4, g);
    ASSERT_EQ(e4.second, true);

    e1 = edge(v4, v1, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v4, v2, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v4, v3, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v3, v4, g);
    ASSERT_EQ(e4.second, false);
}

TYPED_TEST(GraphFixture, myTest40) {
    using graph_type        = typename TestFixture::graph_type;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    //v3 doesn't have adjacent vertex and not connected to any vertices
    add_edge(v2, v1, g);
    add_edge(v1, v2, g);
    add_edge(v1, v4, g);
    add_edge(v2, v4, g);
    add_edge(v4, v1, g);
    add_edge(v4, v2, g);


    pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    ASSERT_EQ(e1.second, true);
    pair<edge_descriptor, bool> e2 = edge(v2, v1, g);
    ASSERT_EQ(e2.second, true);
    pair<edge_descriptor, bool> e3 = edge(v4, v1, g);
    ASSERT_EQ(e3.second, true);
    pair<edge_descriptor, bool> e4 = edge(v4, v2, g);
    ASSERT_EQ(e4.second, true);

    e1 = edge(v3, v1, g);
    ASSERT_EQ(e1.second, false);
    e2 = edge(v3, v2, g);
    ASSERT_EQ(e2.second, false);
    e3 = edge(v1, v3, g);
    ASSERT_EQ(e3.second, false);
    e4 = edge(v3, v4, g);
    ASSERT_EQ(e4.second, false);
    e1 = edge(v2, v3, g);
    ASSERT_EQ(e1.second, false);
    e1 = edge(v3, v3, g);
    ASSERT_EQ(e1.second, false);
    e1 = edge(v4, v3, g);
    ASSERT_EQ(e1.second, false);
}