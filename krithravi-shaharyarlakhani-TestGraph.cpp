// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
            Graph // uncomment, add a comma to the line above
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}


TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    adjacency_iterator b1 = adjacent_vertices(vdA, g).first;
    adjacency_iterator e1 = adjacent_vertices(vdA, g).second;

    adjacency_iterator b2 = adjacent_vertices(vdB, g).first;
    adjacency_iterator e2 = adjacent_vertices(vdB, g).second;

    ASSERT_NE(b1, e1);
    ASSERT_EQ(b2, e2);
}

TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    auto ed = edge(vdA, vdB, g);

    ASSERT_EQ(ed.second, false);
    // std::cout << "NON EXISTENT EDGE DESCRIPTOR : " << ed.first << std::endl;
}

TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    auto ed = edge(vdA, vdB, g);

    // std::cout << "NON EXISTENT SOURCE : " << source(ed.first, g) << std::endl;
    // std::cout << "NON EXISTENT TARGET : " << target(ed.first, g) << std::endl;

    ASSERT_EQ(ed.second, false);
    // std::cout << "NON EXISTENT EDGE DESCRIPTOR : " << ed.first << std::endl;
}

// cyclical graph, 3 v, 3 e
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);

	ASSERT_EQ(num_edges(g), 3);	
	ASSERT_EQ(num_vertices(g), 3);	

}

TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);
	vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);

	auto edgIt = edges(g);
	auto b = edgIt.first;
	auto e = edgIt.second;

	while (b != e) {
		ASSERT_NE(source(*b, g), vdD);
        ASSERT_NE(target(*b, g), vdD);
		++b;
	}	
}

TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);
	vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);


	ASSERT_EQ(num_edges(g), 3);	
	ASSERT_EQ(num_vertices(g), 4);	
}

TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdA, vdC, g);
	add_edge(vdC, vdA, g);

	auto it = adjacent_vertices(vdA, g);
	ASSERT_EQ(std::distance(it.first, it.second), 2);
}

TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);
	vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);

	auto it = vertices(g);
	ASSERT_EQ(std::distance(it.first, it.second), 4);
}

TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);
	vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);

	auto it = edges(g);
	ASSERT_EQ(std::distance(it.first, it.second), 3);
}

// stress test
TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

	for (int i = 0; i < INT_MAX/1000; i++) {
		add_vertex(g);
	}

	EXPECT_TRUE(num_vertices(g) == INT_MAX/1000);
}

TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
	auto res = add_edge(vdA, vdB, g);

	ASSERT_EQ(res.second, false);

}

TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
	auto res = add_edge(vdB, vdA, g);

	ASSERT_EQ(res.second, true);

}

TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

	vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

	auto res = vertex(0, g);
	ASSERT_EQ(res, vdA);
	ASSERT_NE(res, vdB);


}

TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

	vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

	auto res = vertex(1, g);
	ASSERT_EQ(res, vdB);
	ASSERT_NE(res, vdA);
}

TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

	vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
	vdC = vdB;

	ASSERT_EQ(vertex(0, g), vdA);
	ASSERT_EQ(vertex(1, g), vdB);
	ASSERT_EQ(vertex(1, g), vdC);
}

TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
	vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

	graph_type g1(g);
	ASSERT_EQ(num_vertices(g1), num_vertices(g));

}

TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
	vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    auto ed = add_edge(vdA, vdB, g).first;
	ASSERT_EQ(source(ed, g), vdA);
	ASSERT_EQ(target(ed, g), vdB);

}



TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    auto a = add_vertex(g);
    auto b = add_vertex(g);
    add_edge(a, a, g);
    add_edge(b, b, g);
    add_edge(a, b, g);
    add_edge(b, a, g);
    
    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_EQ(num_edges(g), 4);
}

TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    auto a = add_vertex(g);
    add_edge(a, a, g);
    
    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    for (int i = 0; i < 10; ++i) 
        add_vertex(g);

    auto vs = vertices(g);
    auto b = vs.first + 1;
    auto e = vs.second;
    auto prev = vs.first;

    while (b != e) {
        ASSERT_NE(*prev, *b);
        prev = b;
        ++b;
    }
}

TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    for (int i = 0; i < 10; ++i) 
        add_vertex(g);

    auto vs = vertices(g);
	ASSERT_EQ(std::distance(vs.first, vs.second), 10);
}

TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    auto gA = add_vertex(g);
    add_edge(gA, gA, g);
    auto es = edges(g);
    auto b = es.first;
    auto e = es.second;
    while(b != e) {
        ASSERT_EQ(source(*b, g), gA);
        ASSERT_EQ(target(*b, g), gA);
        ++b;
    }
}

TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    auto gA = add_vertex(g);
    auto gB = add_vertex(g);
    add_edge(gA, gB, g);

	ASSERT_EQ(add_edge(gA, gB, g).second, false);
}

TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    auto gA = add_vertex(g);
    auto gB = add_vertex(g);

	ASSERT_EQ(add_edge(gA, gB, g).second, true);
}

TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    auto gA = add_vertex(g);
    auto gB = add_vertex(g);

	ASSERT_EQ(edge(gA, gB, g).second, false);
}

TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    
    auto gA = add_vertex(g);
    auto gB = add_vertex(g);
    add_edge(gA, gB, g);

	ASSERT_EQ(edge(gA, gB, g).second, true);
}

TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    graph_type g2;

    add_vertex(g);

	ASSERT_NE(num_vertices(g), num_vertices(g2));
}

TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    graph_type g2;

    auto temp = add_vertex(g);
    add_edge(temp, temp, g);

	ASSERT_NE(num_edges(g), num_edges(g2));
}

TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    graph_type g2;

	ASSERT_EQ(num_edges(g), num_edges(g2));
}

TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    graph_type g2;

	ASSERT_EQ(num_vertices(g), num_vertices(g2));
}

TYPED_TEST(GraphFixture, test36) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    auto e = add_edge(vdA, vdA, g).first;

	ASSERT_EQ(source(e, g), target(e, g));
}

TYPED_TEST(GraphFixture, test37) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdA, g);

    auto it1 = adjacent_vertices(vdA, g);
    auto it2 = adjacent_vertices(vdB, g);

	ASSERT_EQ(std::distance(it1.first, it1.second), std::distance(it2.first, it2.second));
}

TYPED_TEST(GraphFixture, test38) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);
	vertex_descriptor vdD = add_vertex(g);

	auto edgIt = edges(g);
	auto b = edgIt.first;
	auto e = edgIt.second;

	ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
	vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
	add_edge(vdB, vdC, g);
	add_edge(vdC, vdA, g);

	ASSERT_EQ(add_edge(vdA, vdB, g).second, false);
}

TYPED_TEST(GraphFixture, test40) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;
    auto a = add_vertex(g);
    auto b = add_vertex(g);
    add_edge(a, a, g);
    add_edge(b, b, g);
    add_edge(a, b, g);
    
    ASSERT_EQ(edge(a, a, g).second, true);
    ASSERT_EQ(edge(a, b, g).second, true);
    ASSERT_EQ(edge(b, b, g).second, true);
    ASSERT_EQ(edge(b, a, g).second, false);
}
